#!/usr/bin/env python
import os
import sys

# Add the project to the path
this_dir = os.path.dirname(os.path.dirname(__file__))
sys.path.insert(0, this_dir)
sys.path.insert(0, os.path.join(this_dir, 'externals', 'django-ajaxify', 'ajaxify')) # TODO: We need to split ajaxify off into a separate installable package. This is hacky.
sys.path.insert(0, os.path.join(this_dir, 'apps'))

# setup the environment
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.local')
os.environ.setdefault('PYTHON_EGG_CACHE', '/tmp')

from django.core.management import execute_from_command_line
execute_from_command_line()
