$(function() {
	//$('.form-item .orderable');
	$('.form-item.inline .row').last().each(function(i) {
		// Convert the last 'extra' field row in inline form into a hidden prototype to clone
		$(this).after(newInlineForm(this, false));
		$(this).addClass("prototype").hide();
	});

	$('.form-item-adder-button').click(function() {
		// When an inline form's adder is clicked...
		// Add new row to the inline form dom
		var newForm = newInlineForm($(this).siblings(".row.prototype"), true);
		$(this).before(newForm.show());
    });

	// Deal with Django labels, to try to modernize them into placeholders.
    $(function() {
    	// Convert django's labels into placeholders.
    	$('label').each(function(i, v) {
    		var field_id = $(v).attr('for');
    		var placeholder = $(v).text();

    		if (field_id != '' && field_id != null && field_id != undefined &&
    			placeholder != '' && placeholder != null && placeholder != undefined) {

    			// Find the form field the label is for, and add the label text as its placeholder
    			var form_field = $(v).siblings('#'+field_id).get()
    			$(form_field).attr('placeholder', placeholder);

    			// We exclude some form field types from having the label hidden because
	    		// placeholders aren't displayed with them. E.G. select fields.
	    		var input_tag_name = $(form_field).prop('tagName');
	    		if (input_tag_name != 'SELECT' && $(form_field).attr('type') != 'file' && $(form_field).attr('type') != 'checkbox') {
					$(v).hide();
    			}
    		}
    	});

    	if (!Modernizr.forms_placeholder) {
    		// Emulate placeholder support.
    		$('input, textarea').placeholder();
    	}
 		
	});
});

function newInlineForm(proto, do_add_to_total) {
	// Clones a prototype empty inline item of an inline form
	var newForm = $(proto).clone().removeClass("prototype").addClass("new");
	var managementForm = $(proto).siblings(".form-inline-management");
	var totalElem = $(managementForm).find("input[id$='TOTAL_FORMS']");
	var total = parseInt(totalElem.val());
	if (do_add_to_total == true) {
		total += 1;
		totalElem.val(total);
	}

	// Extract the inline form prefix from a management form element
	// TODO: is the management form guaranteed to exist on inline forms/formsets?
	var prefix = $(totalElem).attr("name");
	prefix = prefix.substring(0, prefix.indexOf("TOTAL_FORMS"));

	newForm.find('label[for*="DELETE"]').hide();
    newForm.find('input[name*="DELETE"]').hide().after(
    	$('<button type="button" class="form-item-deleter-button">Delete</button>').click(function() {
    		// When an inline form item's deleter is clicked...

    		// Update the management form total with -1
			totalElem.val(parseInt(totalElem.val()) - 1);

    		var row = $(this).parent().parent();
    		var rowparent = row.parent();
    		row.remove();
    		rowparent.find(".row").each(function(i) {
				// Convert the last 'extra' field row in inline form into a hidden prototype to clone
				$(this).find("*").each(function () {
    				updateElementIndex(this, prefix, i-1);
    			});
			});
    	})
	);

    newForm.find("*").each(function () {
    	updateElementIndex(this, prefix, total-1);
    });

	return newForm;
}

function updateElementIndex(el, prefix, ndx) {
	var id_regex = new RegExp('(' + prefix + '\\d+)');
	var replacement = prefix + ndx;
	if ($(el).attr("for")) $(el).attr("for", $(el).attr("for").replace(id_regex, replacement));
	if (el.id) el.id = el.id.replace(id_regex, replacement);
	if (el.name) el.name = el.name.replace(id_regex, replacement);
}
