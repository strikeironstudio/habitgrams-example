"""Development settings and globals."""


from os.path import join, normpath, abspath

from base import *

DEBUG = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
TEMPLATE_DEBUG = DEBUG

HOSTNAME = 'http://127.0.0.1:8000'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        #'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        "ENGINE": "django.db.backends.sqlite3",
        'NAME': '/Users/fil/Documents/habitgrams.db', # Or path to database file if using sqlite3.
        #'NAME': 'habitgram', # Or path to database file if using sqlite3.
        #'USER': 'postgres', # Not used with sqlite3.
        #'PASSWORD': 'postpass', # Not used with sqlite3.
        #'HOST': 'localhost', # Set to empty string for localhost. Not used with sqlite3.
        #'PORT': '5432', # Set to empty string for default. Not used with sqlite3.
    }
}

# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}


########## TOOLBAR CONFIGURATION
# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INSTALLED_APPS += (
    'debug_toolbar',
)

########## PAYPAL CONFIGURATION
#PAYPAL_API_URL = 'https://api-3t.sandbox.paypal.com/nvp'
#PAYPAL_LOGIN_URL = (
#    'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='
#)
########## END PAYPAL CONFIGURATION

# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INTERNAL_IPS = ('127.0.0.1',)

# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)
# See: https://github.com/django-debug-toolbar/django-debug-toolbar
DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}
########## END TOOLBAR CONFIGURATION
