from distutils.core import setup

with open('README.md') as readme_f:
    long_description = readme_f.read()

kwargs = {
    'name': 'django-ajaxify',
    'version': '0.1.0',
    'description': 'An ajaxifying forms helper for django projects',
    'long_description': long_description,
    'author': 'AndrewBC',
    'author_email': 'andrew.b.coleman@gmail.com',
    'url': 'https://github.com/AndrewBC/django-ajaxify',
    'download_url': 'https://github.com/AndrewBC/django-ajaxify/downloads',
    'license': 'MIT',
    'platforms': ['Python'],
    'classifiers': [
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Topic :: Software Development :: Build Tools'
        ],
    'packages': ['ajaxify', 'ajaxify.templatetags'],
    'package_data': {
        'ajaxify': ['templates/ajaxify/*.js'],
    },
    'install_requires': [
        "Django >= 1.1.1",
    ],
}

setup(**kwargs)
