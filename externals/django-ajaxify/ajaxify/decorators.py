import functools

from ._core import functions


def ajaxify(*dargs, **dkwargs):
    """ Register some function as an ajax function

    For legacy purposes, if only a function is passed register it a simple
    single ajax function using POST, i.e:

    @ajaxify
    def ajax_function(request):
        ...

    ajaxify allows you to customize the http method and the final name
    of the registered function. E.G.

    @ajaxify(method='GET')
    def ajax_function(request):
        ...

    @ajaxify(method='GET', name='my.custom.name')
    def ajax_function(request):
        ...

    You can also register the same function to use a different http method
    and/or use a different name.

    @ajaxify(method='GET', name='users.get')
    @ajaxify(method='POST', name='users.update')
    def ajax_function(request):
        ...
    """

    if len(dargs) and not dkwargs:
        function = dargs[0]
        functions.register(function)
        return function

    def decorator(function):
        @functools.wraps(function)
        def wrapper(request, *args, **kwargs):
            return function(request, *args, **kwargs)
        functions.register(function, *dargs, **dkwargs)
        return wrapper
    return decorator
