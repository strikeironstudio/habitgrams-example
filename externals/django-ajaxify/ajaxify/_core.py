from django.utils import simplejson as json
from django.utils.importlib import import_module
from django.http import QueryDict

import logging

log = logging.getLogger('ajaxify')

from django.conf import settings

def deserialize_form(data):
    """
    Create a new QueryDict from a serialized form.
    """
    return QueryDict(query_string=unicode(data).encode('utf-8'))


class Function(object):
    """ Basic representation of an ajax function."""

    def __init__(self, function, name, method):
        self.function = function
        self.name = name
        self.method = method

    def call(self, *args, **kwargs):
        """ Call the function. """
        return self.function(*args, **kwargs)


class Module(object):
    """ Basic representation of an ajax module. """

    def __init__(self, name=None):
        self.name = name
        self.functions = {}
        self.submodules = {}

    def add(self, name, function):
        """ Add this function at the ``name`` deep. If the submodule already
        exists, recusively call the add method into the submodule. If not,
        create the module and call the add method."""

        # If this is not the final function name (there are more modules)
        # split the name again an register a new submodule.
        if '.' in name:
            module, extra = name.split('.', 1)
            if module not in self.submodules:
                self.submodules[module] = Module(module)
            self.submodules[module].add(extra, function)
        else:
            self.functions[name] = function


class FunctionRegistry(object):
    def __init__(self):
        self._registry = {}
        self._modules = None

    def register(self, function, name=None, method='POST'):
        """
        Register this function as an ajax function.

        If no name is provided, the module and the function name will be used.
        The final (customized or not) must be unique. """

        method = self.clean_method(method)

        # Generate a default name
        if not name:
            module = ''.join(str(function.__module__).rsplit('.ajax', 1))
            name = '.'.join((module, function.__name__))

        if ':' in name:
            log.error('Ivalid function name %s.' % name)
            return

        # Check for already registered functions
        if name in self._registry:
            log.error('%s was already registered.' % name)
            return

        # Create the dajaxice function.
        function = Function(function=function, name=name, method=method)

        # Register this new ajax function
        self._registry[name] = function

    def is_callable(self, name, method):
        """ Return if the function callable or not. """
        return name in self._registry and self._registry[name].method == method

    def clean_method(self, method):
        """ Clean the http method. """
        method = method.upper()
        if method not in ['GET', 'POST']:
            method = 'POST'
        return method

    def get(self, name):
        """ Return the ajaxify function."""
        return self._registry[name]

    @property
    def modules(self):
        """ Return an easy to loop module hierarchy with all the functions."""
        if not self._modules:
            self._modules = Module()
            for name, function in self._registry.items():
                self._modules.add(name, function)
        return self._modules

class Ajax(object):

    def __init__(self):
        self.calls = [] 

    def json(self):
        return json.dumps(self.calls)

    def alert(self, message):
        self.calls.append({'cmd': 'alert', 'val': message})

    def assign(self, id, attribute, value):
        self.calls.append({'cmd': 'as', 'id': id, 'prop': attribute, 'val': value})

    def add_css_class(self, id, value):
        if not hasattr(value, '__iter__'):
            value = [value]
        self.calls.append({'cmd': 'addcc', 'id': id, 'val': value})

    def remove_css_class(self, id, value):
        if not hasattr(value, '__iter__'):
            value = [value]
        self.calls.append({'cmd': 'remcc', 'id': id, 'val': value})

    def append(self, id, attribute, value):
        self.calls.append({'cmd': 'ap', 'id': id, 'prop': attribute, 'val': value})

    def prepend(self, id, attribute, value):
        self.calls.append({'cmd': 'pp', 'id': id, 'prop': attribute, 'val': value})

    def clear(self, id, attribute):
        self.calls.append({'cmd': 'clr', 'id': id, 'prop': attribute})

    def redirect(self, url, delay=0):
        self.calls.append({'cmd': 'red', 'url': url, 'delay': delay})

    def script(self, code):  # OK
        self.calls.append({'cmd': 'js', 'val': code})

    def remove(self, id):
        self.calls.append({'cmd': 'rm', 'id': id})

    def add_data(self, data, function):
        self.calls.append({'cmd': 'data', 'val': data, 'fun': function})


class Config(object):
    """ Provide an easy to use way to read the ajaxify configuration and
    return the default values if no configuration is present."""

    default_config = {'AJAXIFY_XMLHTTPREQUEST_JS_IMPORT': True,
                      'AJAXIFY_JSON2_JS_IMPORT': True,
                      'AJAXIFY_EXCEPTION': 'AJAXIFY_EXCEPTION',
                      'AJAXIFY_MEDIA_PREFIX': 'ajaxify'}

    def __getattr__(self, name):
        """ Return the customized value for a setting (if it exists) or the
        default value if not. """

        if name in self.default_config:
            if hasattr(settings, name):
                return getattr(settings, name)
            return self.default_config.get(name)
        return None

    @property
    def ajaxify_url(self):
        return r'^%s/' % self.AJAXIFY_MEDIA_PREFIX

    @property
    def django_settings(self):
        return settings

    @property
    def modules(self):
        return functions.modules


functions = FunctionRegistry()
config = Config()
