from django.conf.urls.defaults import *
from .views import AjaxifyRequest

urlpatterns = patterns('ajaxify.views',
    url(r'^(.+)/$', AjaxifyRequest.as_view(), name='ajaxify-call-endpoint'),
    url(r'', AjaxifyRequest.as_view(), name='ajaxify-endpoint'),
)
