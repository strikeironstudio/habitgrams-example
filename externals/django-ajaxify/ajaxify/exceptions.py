class AjaxifyError(Exception):
    pass


class AjaxifyFunctionNotCallableError(AjaxifyError):
    pass


class AjaxifyImportError(AjaxifyError):
    pass
