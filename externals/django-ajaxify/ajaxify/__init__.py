__version__ = (0, 1, 0, 'alpha')

from ._core import functions, config, Ajax, deserialize_form
from .decorators import ajaxify