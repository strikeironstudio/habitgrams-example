import logging

from django.conf import settings
from django.utils import simplejson
from django.views.generic.base import View
from django.http import HttpResponse, Http404

from .exceptions import AjaxifyFunctionNotCallableError
from ._core import functions, config

log = logging.getLogger('ajaxify')


def safe_dict(d):
    """
    Recursively clone json structure with UTF-8 dictionary keys
    http://www.gossamer-threads.com/lists/python/bugs/684379
    """
    if isinstance(d, dict):
        return dict([(k.encode('utf-8'), safe_dict(v)) for k, v in d.iteritems()])
    elif isinstance(d, list):
        return [safe_dict(x) for x in d]
    else:
        return d


class AjaxifyRequest(View):
    """ Handle all the ajaxify xhr requests. """

    def dispatch(self, request, name=None):

        if not name:
            raise Http404

        # Check if the function is callable
        if functions.is_callable(name, request.method):
            function = functions.get(name)
            data = getattr(request, function.method).get('argv', '')

            # Clean the argv
            if data != 'undefined':
                try:
                    data = safe_dict(simplejson.loads(data))
                except Exception:
                    data = {}
            else:
                data = {}

            # Call the function. If something goes wrong, handle the Exception
            try:
                response = function.call(request, **data)
            except Exception:
                if settings.DEBUG:
                    raise
                response = config.AJAXIFY_EXCEPTION

            return HttpResponse(response, mimetype="application/x-json")
        else:
            raise AjaxifyFunctionNotCallableError(name+' VALID names are: '+str(functions._registry))
