{{ name }}: {
    {% include "ajaxify/ajaxify_function_loop.js" %}
    {% with parent_foorloop=forloop %}
    {% for name, sub_module in module.submodules.items %}
    {% with filename="ajaxify/ajaxify_module_loop.js" module=sub_module %}
        {% include filename %}
    {% endwith %}
    {% if not forloop.last %},{% endif %}
    {% endfor %}
    }
    {% endwith %}
