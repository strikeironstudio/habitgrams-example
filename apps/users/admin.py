# users/admin.py

from django.contrib import admin

from .models import (BillingInfo,
                     SchedulePreferences,
                     HabitgramUser,
                     TimeSlot)

admin.site.register(BillingInfo)
admin.site.register(SchedulePreferences)
admin.site.register(HabitgramUser)
admin.site.register(TimeSlot)
