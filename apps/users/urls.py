# users/urls.py
from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required

from .views import (SettingsView,)

urlpatterns = patterns('',
     url(r'^settings/$', login_required(SettingsView.as_view()), name='user-settings'),
)
