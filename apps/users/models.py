# users/models.py

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.db import models

import recurly
import re

from core.models.base import TimeStampedModel
from core.utils import PREFERENCE_OPTION_TIMES

from django.db.models.signals import post_save
from django.dispatch import receiver

import pytz
from timezone_field import TimeZoneField

User = get_user_model()

class HabitgramUser(TimeStampedModel):
    """
        Habitgram version of a User, tying into django built in user model
    """
    
    user = models.ForeignKey(User, null=True, blank=True)
    first_name = models.CharField(default='', max_length=50, null=True, blank=True)
    last_name = models.CharField(default='', max_length=50, null=True, blank=True)
    phone_number = models.CharField(default='', max_length=255, null=True, blank=True)
    company_name = models.CharField(default='', max_length=50, null=True, blank=True)
    language_code = models.CharField(default='', max_length=3, null=True, blank=True)
    address = models.CharField(default='', max_length=255, null=True, blank=True)
    address2 = models.CharField(default='', max_length=255, null=True, blank=True)
    city = models.CharField(default='', max_length=50, null=True, blank=True)
    state = models.CharField(default='', max_length=50, null=True, blank=True)
    country = models.CharField(default='', max_length=2, null=True, blank=True)
    zip = models.CharField(default='', max_length=10, null=True, blank=True)
    timezone = TimeZoneField(default='US/Pacific')
    
    subscriptions = models.ManyToManyField('programs.Habitgram', through='programs.Subscription')

    @classmethod
    def get_by_user_id(cls, user_id):
        try:
            user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            raise ValidationError("User does not exist in the db")
        else:
            habitgram_user = None
            try:
                habitgram_user = cls.objects.get(user=user)
            except cls.DoesNotExist:
                raise ValidationError("Habitgram user does not exist in the db")
            except Exception, e: raise ValidationError('%s' % e)
            else: return habitgram_user if habitgram_user \
                else "Not a Habitgram user"
    
    def check_phone_number_format(self):
        result = []
        pattern = re.compile(r'^\+(\d+)$')
        matches = re.findall(pattern, self.phone_number)
        if matches or self.phone_number == '':
            result.append('correct')
        else:
            result.append('error')
            result.append('Phone number should be of the form +4676447536')
        return result
    
    def validate_fields(self):
        errors = []
        # Check valid phone number
        validate_phone_number = self.check_phone_number_format()
        if validate_phone_number[0] != 'correct':
            errors.append(validate_phone_number)
        
        if errors == []: return True
        else: return errors
    
    def save(self, *args, **kwargs):
        valid_fields = self.validate_fields()
        if valid_fields != True: raise ValidationError(valid_fields)
        super(HabitgramUser, self).save(*args, **kwargs)
        # TO DO:
        ## Save phone number in Twilio
        # Save account details in Recurly
        if settings.RECURLY_ONLINE:
            try:
                account = recurly.Account.get(self.get_recurly_account_code())
            except recurly.NotFoundError:
                account = recurly.Account(account_code=self.get_recurly_account_code())
            account.email = self.user.email
            account.first_name = self.first_name
            account.last_name = self.last_name
            account.company_name = self.company_name
            account.address = self.address
            account.save()
    
    def get_recurly_account_code(self):
        return '%(id)s' % {'id': str(self.user.id)} 
    
    def get_settings_url(self):
        return reverse('subscriber_settings', args=[str(self.user.id)])
    
    def __unicode__(self):
        name =  '%s (%s %s)' % (str(self.user), self.first_name, self.last_name)
        if len(name) < 1:
            name = "UNKNOWN_USER"
        return name

@receiver(post_save, sender=User)
def auto_create_habitgram_user(sender, instance, signal, created, **kwargs):
    if created:
        huser = HabitgramUser(user=instance)
        huser.save()

class BillingInfo(models.Model):
    user = models.ForeignKey(User,
                             null=False,
                             blank=False)
    first_name = models.CharField(default='',
                                  max_length=50,
                                  null=False,
                                  blank=False)
    last_name = models.CharField(default='',
                                  max_length=50,
                                  null=False,
                                  blank=False)
    address1 = models.CharField(default='',
                                  max_length=255,
                                  null=False,
                                  blank=False)
    address2 = models.CharField(default='',
                                  max_length=255,
                                  null=True,
                                  blank=True)
    city = models.CharField(default='',
                                  max_length=50,
                                  null=False,
                                  blank=False)
    state = models.CharField(default='',
                                  max_length=50,
                                  null=True,
                                  blank=True)
    country = models.CharField(default='',
                                  max_length=2,
                                  null=False,
                                  blank=False)
    zip = models.CharField(default='',
                                  max_length=10,
                                  null=False,
                                  blank=False)
    phone = models.CharField(default='',
                                  max_length=50,
                                  null=True,
                                  blank=True)
    vat_number = models.CharField(default='',
                                  max_length=50,
                                  null=True,
                                  blank=True)
    ip_address = models.GenericIPAddressField(default='',
                                  max_length=50,
                                  null=True,
                                  blank=True)
    number = models.CharField(default='',
                              max_length=16,
                              null=False,
                              blank=False)
    expire_month = models.CharField(default='',
                              max_length=2,
                              null=False,
                              blank=False)
    expire_year = models.CharField(default='',
                              max_length=4,
                              null=False,
                              blank=False)
    cvv_number = models.CharField(default='',
                              max_length=4,
                              null=False,
                              blank=False)
    
    def save(self, *args, **kwargs):
        if settings.RECURLY_ONLINE:
            try:
                subscriber = HabitgramUser.get_by_user_id(self.user.id)
                account = recurly.Account.get(subscriber.get_recurly_account_code())
            except recurly.NotFoundError:
                raise ValidationError('Please fill in your contact information first')
            else:
                super(BillingInfo, self).save(*args, **kwargs)
                # Save billing info in Recurly
                account.address1 = self.address1
                account.address2 = self.address2
                account.city = self.city
                account.zip = self.zip
                account.country = self.country
                account.billing_info = recurly.BillingInfo(
                                       first_name=self.first_name,
                                       last_name=self.last_name,
                                       number=self.number,
                                       verification_value=self.cvv_number,
                                       month=self.expire_month,
                                       year=self.expire_year,
                                       
                                       address1=self.address1,
                                       address2=self.address2,
                                       city=self.city,
                                       state=self.state,
                                       zip=self.zip,
                                       country=self.country)
                account.save()
    
    @classmethod
    def get_billing_info_by_user_id(cls, user_id):
        try:
            user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            raise ValidationError("User does not exist in the db")
        else:
            billing_info = None
            try:
                billing_info = cls.objects.get(user=user)
            except cls.DoesNotExist:
                raise ValidationError("Billing Info does not exist in the db")
            except Exception, e: raise ValidationError('%s' % e)
            else: return billing_info if billing_info else "Not a billing info"
    
    def __unicode__(self):
        return '%(username)s' % \
            {'username': self.user.username}


class TimeSlot(TimeStampedModel):
    """
        A time-slot model consisting of juest time-slots during the day
    """
    slot = models.TimeField(unique=True,
                            choices=PREFERENCE_OPTION_TIMES,
                            null=True,
                            blank=True)
    
    def __unicode__(self):
        return '%(slot)s' % {'slot': self.slot}


class SchedulePreferences(TimeStampedModel):
    user = models.ForeignKey(User)
    preferences = models.ManyToManyField(TimeSlot)
    
    @classmethod
    def get_schedule_info_by_user_id(cls, user_id):
        try:
            user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            raise ValidationError("User does not exist in the db")
        else:
            schedule_info = None
            try:
                schedule_info = cls.objects.get(user=user)
            except cls.DoesNotExist:
                raise ValidationError("Schedule info does not exist in the db")
            except Exception, e: raise ValidationError('%s' % e)
            else: return schedule_info if schedule_info else "Not a schedule info"
    
    def __unicode__(self):
        return self.user.username