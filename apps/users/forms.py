# users/forms.py

from calendar import monthrange
from datetime import date
from django import forms
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import redirect

from .models import (BillingInfo,
                     SchedulePreferences,
                     HabitgramUser,
                     User)


class SubscriberFormMixin(object):
    """
       A mixin for subscriber-related actions
    """
    
    initial = {'key': 'value'}
    subscriber = None
    contact_info_form_class = None
    billing_info_form_class = None
    schedule_info_form_class = None

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        return self.initial.copy()

    def get_contact_info_form_class(self):
        """
        Returns the contact info form class to use in this view
        """
        return self.contact_info_form_class
    
    def get_billing_info_form_class(self):
        """
        Returns the billing info form class to use in this view
        """
        return self.billing_info_form_class
    
    def get_schedule_info_form_class(self):
        """
        Returns the schedule info form class to use in this view
        """
        return self.schedule_info_form_class

    def get_form(self, form_class):
        """
        Returns an instance of the form to be used in this view.
        """
        return form_class(**self.get_form_kwargs())

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """
        kwargs = {'initial': self.get_initial()}
        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def form_invalid(self, **kwargs):
        """
        If the form is invalid, re-render the context data with the
        data-filled form and errors.
        """
        return self.render_to_response(self.get_context_data(**kwargs))
    
    def set_subscriber(self, user):
        self.subscriber = HabitgramUser.get_by_user_id(user.id)

class SubscriberSettingsFormMixin(object):
    """
       A mixin for subscriber-settings-related actions
    """
    subscriber = None
    
    def get_success_url(self):
        """Returns the url of the registration success page."""
        return reverse(self.subscriber.get_settings_url())
    
    def get_error_url(self):
        """Returns the url of the registration error page."""
        return reverse('thankyou')


class ContactInfoForm(forms.ModelForm, SubscriberSettingsFormMixin):
    """
       Form to handle subscriber's contact information
    """
    action = reverse_lazy('user-settings')
    submit_text = "Save Account Information"

    class Meta:
       model = HabitgramUser
       fields = ['first_name', 'last_name', 'phone_number',
                 'company_name', 'address', 'address2',
                 'city', 'state', 'zip', 'country', 'timezone']

       help_texts = {
            'first_name': 'Maximum 50 characters.',
            'last_name': 'Maximum 50 characters.',
            'company_name': 'Maximum 50 characters.',
            'address': 'Maximum 255 characters.',
            'address2': 'Maximum 255 characters.',
            'city': 'Maximum 50 characters.',
            'state': 'Maximum 50 characters.',
            'country': 'Maximum 2 characters.',
            'zip': 'Maximum 10 characters.',
       }
       error_messages = {
            'first_name': {
                'max_length': "This first name is too long (exceeds 50 characters).",
            },
            'last_name': {
                'max_length': "This last name is too long (exceeds 50 characters).",
            },
            'company_name': {
                'max_length': "This company name is too long (exceeds 50 characters).",
            },
            'address': {
                'max_length': "This address is too long (exceeds 255 characters).",
            },
            'address2': {
                'max_length': "This address (part 2) is too long (exceeds 255 characters).",
            },
            'city': {
                'max_length': "This city name is too long (exceeds 50 characters).",
            },
            'state': {
                'max_length': "This state name is too long (exceeds 50 characters).",
            },
            'country': {
                'max_length': "This country name is too long (exceeds 2 characters).",
            },
            'zip': {
                'max_length': "This zip is too long (exceeds 10 characters).",
            },
       }
    
    def save_contact_info(self, user, subscriber):
        self.subscriber = subscriber
        self.subscriber.first_name = user.first_name = self.data['first_name']
        self.subscriber.last_name = user.last_name = self.data['last_name']
        self.subscriber.company_name = self.data['company_name']
        self.subscriber.address = self.data['address']
        self.subscriber.address2 = self.data['address2']
        self.subscriber.city = self.data['city']
        self.subscriber.state = self.data['state']
        self.subscriber.country = self.data['country']
        self.subscriber.zip = self.data['zip']
        self.subscriber.phone_number = self.data['phone_number']
        user.full_clean()
        self.subscriber.timezone = self.data['timezone']
        user.save()
        self.subscriber.save()


class ScheduleForm(forms.ModelForm, SubscriberSettingsFormMixin):
    """
       Form to handle user's habitgram-schedule
    """
    class Meta:
        model = SchedulePreferences
        fields = ['preferences']
    
    def save_schedule_info(self, user):
        # Get or create schedule information for user from db
        self.schedule_info = SchedulePreferences.objects.get_or_create(
                                user=user)
        # Initialize preferences
        self.schedule_info.preferences = []
        for preference in self.cleaned_data['preferences']:
            self.schedule_info.preferences.add(preference)
        # Save the schedule information
        self.schedule_info.save()

class CreditCardField(forms.IntegerField):
    def get_cc_type(self, number):
        """
           Gets credit card type given number. Based on values from Wikipedia page
           "Credit card number".
           <a href="http://en.wikipedia.org/w/index.php?title=Credit_card_number<br />
           " title="http://en.wikipedia.org/w/index.php?title=Credit_card_number<br />
           ">http://en.wikipedia.org/w/index.php?title=Credit_card_number<br />
           </a>
        """
        number = str(number)
        #group checking by ascending length of number
        if len(number) == 13:
            if number[0] == "4":
                return "Visa"
        elif len(number) == 14:
            if number[:2] == "36":
                return "MasterCard"
        elif len(number) == 15:
            if number[:2] in ("34", "37"):
                return "American Express"
        elif len(number) == 16:
            if number[:4] == "6011":
                return "Discover"
            if number[:2] in ("51", "52", "53", "54", "55"):
                return "MasterCard"
            if number[0] == "4":
                return "Visa"
        return "Unknown"
 
    def clean(self, value):
        """
           Check if given CC number is valid and one of the
           card types we accept
        """
        if value and (len(value) < 13 or len(value) > 16):
            raise forms.ValidationError("Please enter in a valid " + \
                                        "credit card number.")
        elif self.get_cc_type(value) not in ("Visa", "MasterCard",
                                             "American Express", "Discover"):
            raise forms.ValidationError("Please enter in a Visa, " + \
                                        "Master Card, Discover, or " \
                                        "American Express credit card number.")
 
        return super(CreditCardField, self).clean(value)
 
class PaymentForm(forms.ModelForm):
    number = CreditCardField(required=True, label="Card Number")
    first_name = forms.CharField(required=True, label="Card Holder First Name", max_length=30)
    last_name = forms.CharField(required=True, label="Card Holder Last Name", max_length=30)
    expire_month = forms.ChoiceField(required=True, choices=[(x, x) for x in xrange(1, 13)])
    expire_year = forms.ChoiceField(required=True, choices=[(x, x) for x in xrange(date.today().year, date.today().year + 15)])
    cvv_number = forms.IntegerField(required=True, label="CVV Number",
                                    max_value=9999,
                                    widget=forms.TextInput(attrs={'size': '4'}))
    billing_info = None
    
    class Meta:
        model = BillingInfo
        fields = ['number', 'first_name', 'last_name',
                  'expire_month', 'expire_year', 'cvv_number',
                  'address1', 'address2', 'city', 'state', 'country', 'zip']
 
    def __init__(self, *args, **kwargs):
        self.payment_data = kwargs.pop('payment_data', None)
        super(PaymentForm, self).__init__(*args, **kwargs)
 
    def clean(self):
        cleaned_data = super(PaymentForm, self).clean()
        expire_month = cleaned_data.get('expire_month')
        expire_year = cleaned_data.get('expire_year')
        cvv_number = cleaned_data.get('cvv_number')
 
        if expire_year in forms.fields.EMPTY_VALUES:
            self._errors["expire_year"] = self.error_class(["You must select a valid Expiration year."])
            del cleaned_data["expire_year"]
        if expire_month in forms.fields.EMPTY_VALUES:
            self._errors["expire_month"] = self.error_class(["You must select a valid Expiration month."])
            del cleaned_data["expire_month"]
        year = int(expire_year)
        month = int(expire_month)
        # find last day of the month
        day = monthrange(year, month)[1]
        expire = date(year, month, day)
 
        if date.today() > expire:
            #raise forms.ValidationError("The expiration date you entered is in the past.")
            self._errors["expire_year"] = self.error_class(["The expiration date you entered is in the past."])
        
        if len(str(cvv_number)) < 4:
            self._errors["cvv_number"] = self.error_class(["The verification number must be 4 digits."])
 
        return cleaned_data
    
    def save_billing_info(self, user):
        try:
            self.billing_info = BillingInfo.get_billing_info_by_user_id(user.id)
        except:
            self.billing_info = BillingInfo(user=user)
        self.billing_info.number = self.data['number']
        self.billing_info.first_name = user.first_name = self.data['first_name']
        self.billing_info.last_name = user.last_name = self.data['last_name']
        self.billing_info.expire_month = self.data['expire_month']
        self.billing_info.expire_year = self.data['expire_year']
        self.billing_info.cvv_number = self.data['cvv_number']
        
        self.billing_info.address1 = self.data['address1']
        self.billing_info.address2 = self.data['address2']
        self.billing_info.city = self.data['city']
        self.billing_info.state = self.data['state']
        self.billing_info.country = self.data['country']
        self.billing_info.zip = self.data['zip']
        self.billing_info.save()
