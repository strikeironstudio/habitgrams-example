# users/views.py

from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, FormView
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import render

from core.forms import MultiForms

from .forms import (ContactInfoForm,
                    PaymentForm,
                    ScheduleForm,
                    SubscriberFormMixin)

from .models import (HabitgramUser,
                     BillingInfo,
                     SchedulePreferences)

User = get_user_model()


class SettingsView(UpdateView):
    model = HabitgramUser
    form_class = MultiForms
    template_name = 'users/settings.html'

    def get_object(self):
        return HabitgramUser.get_by_user_id(self.request.user.id)

    def get_form_kwargs(self, author=None, initial=None):
        kwargs = super(SettingsView, self).get_form_kwargs()
        kwargs.update({
            'forms': (ContactInfoForm,),
            'action': reverse_lazy("user-settings"),
            'submit_text': "Save Settings",
        })
        
        return kwargs
    
    def get_context_data(self, **kwargs):
        current_user = self.request.user
        habitgram_user = HabitgramUser.get_by_user_id(current_user.id)
        kwargs["pk"] = habitgram_user.id
        context = super(SettingsView, self).get_context_data(**kwargs)
        context["form"] = self.get_form(self.get_form_class())
        return context
    
    def form_valid(self, form):
        """If the form and formsets are valid, save the associated models."""
        subscriber = None
        current_user = self.request.user
        habitgram_user = HabitgramUser.get_by_user_id(current_user.id)

        form.save()
        return HttpResponseRedirect(reverse('user-settings'))
