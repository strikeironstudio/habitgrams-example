from django import forms
from django.core.urlresolvers import reverse, reverse_lazy

from collections import OrderedDict

class BaseForm(forms.Form):
	error_css_class = 'error'
	required_css_class = 'required'

class ContactForm(forms.Form):
    submit_text = "Send Message"
    action = reverse_lazy('home_contact-us')
    subject = forms.CharField(max_length=100,widget=forms.widgets.TextInput(attrs={'placeholder': 'Subject', 'id':'subject'}))
    message = forms.CharField(widget=forms.widgets.Textarea(attrs={'placeholder': 'Message', 'id':'message'}))
   
class MultiForms(object):
	action = "/"
	submit_text = "Submit"
	is_multiform = True

	def __init__(self, **kwargs):
		map(lambda keyvaltup: setattr(self, keyvaltup[0], keyvaltup[1]), kwargs.items())
		forms = []
		data = getattr(self, 'data', None)
		for form in self.forms:
			kwargs = {}
			try:
				if isinstance(self.instance, form.Meta.model):
					kwargs['instance'] = self.instance
			except AttributeError:
				pass

			if data is not None:
				form_item = form(data, **kwargs)
			else:
				form_item = form(**kwargs)

			forms.append(form_item)
		self.forms = forms

	def __delitem__(self, index):
		del self.forms[index]

	def __getitem__(self, index):
		return self.forms[index]

	def __setitem__(self, index, value):
		self.forms[index] = value

	def __iter__(self):
		for form in self.forms:
			yield form

	def __len__(self):
		return len(self.forms)

	def is_valid(self):
		return all([form.is_valid() for form in self.forms])

	def __nonzero__(self):
		return self.is_valid()

	def save(self, commit=True):
		if not commit:
			return [form.save(commit=False) for form in self.forms]

		for form in self:
			try:
				form.save()
			except AttributeError:
				for formset_item in form:
					formset_item.save()
		return tuple()