from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
import ajaxify

from .views import ContactUsView

admin.autodiscover()

urlpatterns = patterns('core.urls',
    url(r'^$',
        TemplateView.as_view(template_name='home.html'),
        name='home_main'),
    url(r'^getting-started/',
        TemplateView.as_view(template_name='getting-started.html'),
        name='home_getting-started'),
    url(r'^author-info/',
        TemplateView.as_view(template_name='author-info.html'),
        name='home_author-info'),
    url(r'^contact-us/',
        login_required(ContactUsView),
        name='home_contact-us'),
    url(r'^do-anything/',
        TemplateView.as_view(template_name='do-anything.html'),
        name='home_do-anything'),
    url(r'^register/',
        TemplateView.as_view(template_name='register.html'),
        name='home_register'),

    # Account registration and authentication
    url(r'^accounts/', include('allauth.urls')),
    # Instructors
    url(r'^users/', include('users.urls')),
    # Courses and programs
    url(r'^habitgrams/', include('programs.urls')),
    # Habitgrams
    #url(r'^habitgrams/', include('habitgrams.urls')),
)


# Default routes
urlpatterns += patterns('',
    # Serve media files
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.STATIC_ROOT,
        }),
    
    
    # Admin docs
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Admin:
    url(r'^admin/', include(admin.site.urls)),

    # ajaxify urls
    url(ajaxify.config.ajaxify_url, include('ajaxify.urls')),
)
