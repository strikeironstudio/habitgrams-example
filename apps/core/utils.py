# core/utils.py
"""
    This file contains the basic yet important constants and functions 
    used in various files throughout this project.
"""

import ast
import datetime
import operator
import re

from django.contrib import admin
from django.core import serializers as django_serializers
from django.db import models



########## CONSTANTS

# GENERAL
DATETIME_FORMAT = '%d %b %I:%M %p'

# HABITGRAMS
HABITGRAM_TYPES = (
    ('EML', 'Email'),
    ('SMS', 'SMS'),
    ('VML', 'Voicemail'),
)

SCHEDULE_OPTIONS = (
    ('Once per day', 'Once per day'),
    ('Twice per day', 'Twice per day'),
    ('Thrice per day', 'Thrice per day'),
    ('Once per week', 'Once per week'),
    ('Twice per week', 'Twice per week'),
    ('Thrice per week', 'Thrice per week'),
)

# TWILIO

CURRENCIES = (
    ('USD', 'USD'),
    ('EUR', 'EUR'),
    ('GBP', 'GBP'),
)

SUBSCRIPTION_PLAN = (
    ('per month', 'per month'),
    ('per year', 'per year'),
    ('per day', 'per day'),
)


# USER SETTINGS

PREFERENCE_OPTIONS = {

    ('09:00', '09:00'),
    ('10:00', '10:00'),
    ('11:00', '11:00'),
    ('12:00', '12:00'),
    ('13:00', '13:00'),
    ('14:00', '14:00'),
    ('15:00', '15:00'),
    ('16:00', '16:00'),
    ('17:00', '17:00'),
    ('18:00', '18:00'),

}

PREFERENCE_OPTION_TIMES = (
    (datetime.time(6, 0), "06:00"),
    (datetime.time(6, 30), "06:30"),
    (datetime.time(7, 0), "07:00"),
    (datetime.time(7, 30), "07:30"),
    (datetime.time(8, 0), "08:00"),
    (datetime.time(8, 30), "08:30"),
    (datetime.time(9, 0), "09:00"),
    (datetime.time(9, 30), "09:30"),
    (datetime.time(10, 0), "10:00"),
    (datetime.time(10, 30), "10:30"),
    (datetime.time(11, 0), "11:00"),
    (datetime.time(11, 30), "11:30"),
    (datetime.time(12, 0), "12:00"),
    (datetime.time(12, 30), "12:30"),
    (datetime.time(13, 0), "13:00"),
    (datetime.time(13, 30), "13:30"),
    (datetime.time(14, 0), "14:00"),
    (datetime.time(14, 30), "14:30"),
    (datetime.time(15, 0), "15:00"),
    (datetime.time(15, 30), "15:30"),
    (datetime.time(16, 0), "16:00"),
    (datetime.time(16, 30), "16:30"),
    (datetime.time(17, 0), "17:00"),
    (datetime.time(17, 30), "17:30"),
    (datetime.time(18, 0), "18:00"),
    )
