from django.shortcuts import render_to_response
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.core.mail import send_mail

from django import forms
from django.conf import settings

from .forms import ContactForm

def ContactUsView(request):
    d = {}
    c = RequestContext(request)
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            d['notifications'] = ('Thank you for your submission. We will respond to you soon!',)
            fail_silently = not settings.DEBUG
            send_mail('[Contact-Us from user: '+str(c['user'])+' -- '+form.cleaned_data['subject'], form.cleaned_data['message'], c['user'].email, [admin[1] for admin in settings.ADMINS], fail_silently=fail_silently)
            form = ContactForm() # An unbound form
    else:
        form = ContactForm() # An unbound form

    d['form'] = form
    return render_to_response('contact-us.html', d, context_instance=c)
