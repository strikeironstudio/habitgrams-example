# core/models/base.py

import re

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models

import recurly

from core.utils import DATETIME_FORMAT

User = get_user_model()

class TimeStampedModel(models.Model):
    """
       An abstract base class model that includes the following fields:
       created,
       modified (self-updating)
    """
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    
    def convert_datetime_to_string(given_datetime):
        """
           Converts a given datetime to the format 03 Mar 12:00 AM
        """
        return given_datetime.strftime(DATETIME_FORMAT)
    
    class Meta:
        abstract = True
