from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required


from .views import (ProgramListView, ProgramAuthListView, ProgramDetailView,
                    ProgramSubscribeView, SubscriptionListView,
                    ProgramCreateView, ProgramUpdateView,
                    program_unsubscribe, SubscriptionUpdateView)

urlpatterns = patterns('',
    url(r'^$', ProgramListView.as_view(), name='program-list'),
    url(r'^subscribed/$', login_required(SubscriptionListView.as_view()), name='program-sub-list'),
    url(r'^authored/$', login_required(ProgramAuthListView.as_view()), name='program-auth-list'),
    url(r'^(?P<pk>\d+)/$', ProgramDetailView.as_view(), name='program-detail'),
    url(r'^create/$', login_required(ProgramCreateView.as_view()), name='program-create'),
    url(r'^edit/(?P<pk>\d+)/$', login_required(ProgramUpdateView.as_view()), name='program-update'),
    url(r'^subscribe/(?P<pk>\d+)/$', login_required(ProgramSubscribeView.as_view()), name='program-subscribe'),
    url(r'^subscription/edit/(?P<pk>\d+)/$', login_required(SubscriptionUpdateView.as_view()), name='program-sub-update'),
    url(r'^unsubscribe/(?P<pk>\d+)/$', login_required(program_unsubscribe), name='program-unsubscribe'),
)
