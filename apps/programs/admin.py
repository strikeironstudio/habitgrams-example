# programs/admin.py

from django.contrib import admin

from .models import Subscription, Habitgram, SubscriptionTime

class SubscriptionTimeInline(admin.StackedInline):
    model = SubscriptionTime
    extra = 1

class SubscriptionAdmin(admin.ModelAdmin):
	model = Subscription
	inlines = [SubscriptionTimeInline,]

admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(Habitgram)
