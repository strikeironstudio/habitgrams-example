# courses/models.py

from datetime import datetime
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.utils.text import slugify
from django.conf import settings
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

import recurly

from twilio.rest import TwilioRestClient
from core.models import TimeStampedModel
from core.utils import CURRENCIES, SUBSCRIPTION_PLAN, HABITGRAM_TYPES, SCHEDULE_OPTIONS
from users.models import (HabitgramUser, BillingInfo)

User = get_user_model()

twilio_client = TwilioRestClient(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)

RECURLY_SMS_PLAN_CODE = 'habitgram_sms_service'
RECURLY_VOICEMAIL_PLAN_CODE = 'habitgram_voicemail_service'

class Habitgram(TimeStampedModel):
    """
       An habitgram class specifying the details of communication with a 
       subscriber
    """

    title = models.CharField(default='', max_length=255)
    description = models.TextField(default='', null=True, blank=True)
    image = models.ImageField(upload_to='images_programs', null=True, blank=True)
    duration = models.CharField(default='',
                                max_length=255)
    price = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    currency = models.CharField(max_length=255,
                                choices=CURRENCIES,
                                default=CURRENCIES[0])
    subscription_plan = models.CharField(max_length=255,
                                      choices=SUBSCRIPTION_PLAN,
                                      default=SUBSCRIPTION_PLAN[0])
    author = models.ForeignKey(HabitgramUser)
    is_active = models.NullBooleanField(default=True)
    message_type = models.CharField(default='EML', max_length=3, choices=HABITGRAM_TYPES)
    message = models.TextField(default='')
    
    def get_recurly_plan_code(self):
        text = '%(id)s-%(title)s' % {'id': str(self.id),
                                     'title': self.title}
        return slugify(text)
    
    def get_absolute_url(self):
        return reverse('program-detail', args=[str(self.id)])
    
    def get_subscription_url(self):
        return reverse('program-subscribe', args=[str(self.id)])
    
    def get_share_url(self):
        return reverse('program-share', args=[str(self.id)])
    
    def delete(self, *args, **kwargs):
        Habitgram.objects.filter(content_type=ContentType.objects.get_for_model(Program),
                                 object_id=self.id).delete()
        super(Program, self).delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        if settings.RECURLY_ONLINE:
            # Save in / update Recurly after saving, if in online mode
            try:
                plan = recurly.Plan.get(self.get_recurly_plan_code())
            except recurly.NotFoundError:
                plan = recurly.Plan(plan_code=self.get_recurly_plan_code())
            plan.name = self.title
            plan.description = self.description
            price_in_cents = int(self.price) * 100
            plan.unit_amount_in_cents = recurly.Money(USD=price_in_cents)
            interval_unit = self.subscription_plan.split(' ')[-1].replace("')", "")
            plan.plan_interval_unit = '%(interval)ss' % {'interval': interval_unit}
            plan.save()

            # Create a recurly subscription  based on the type of habitgram created
            if self.message_type in ('SMS', 'VML'):
                subscription = recurly.Subscription()
                try:
                    billing_info = BillingInfo.objects.get(user=self.content_object.author.user)
                except BillingInfo.DoesNotExist:
                    raise ValidationError('Please enter your billing information')
                try:
                    if self.message_type == 'SMS':
                        subscription.plan_code = RECURLY_SMS_PLAN_CODE
                    elif self.message_type == 'VML':
                        subscription.plan_code = RECURLY_VOICEMAIL_PLAN_CODE
                except recurly.NotFoundError, e:
                    raise ValidationError('Server error: %s\nPlease contact the administrator in the contact page.' % e)
                subscription.currency = self.content_object.currency
                account = recurly.Account.get(self.content_object.author.get_recurly_account_code())
                try:
                    subscription.account = account
                    try:
                        subscription.save()
                    except recurly.ValidationError, e:
                        if e in ['already_subscribed: subscription.base You already have a subscription to this plan.']:
                            super(Subscription, self).save(*args, **kwargs)
                            return
                except recurly.BadRequestError, e:
                    raise ValidationError('Server error: %s\nPlease contact the administrator in the contact page.' % e)
        return super(Habitgram, self).save(*args, **kwargs)
#       self.send()

    
    def send(self, subscriber):
        """Send a habitgram based on the message type"""
        
        if subscriber == None: raise ValidationError('Subscriber cannot be None')
        
        from_author = self.author
        
        if str(self.message_type) == 'EML':
            subject = '%(subject)s' % {'subject': self}
            message = '%(message)s' % {'message': self.message}
            from_email = '%(from_email)s' % {'from_email': from_author.user.email}
            to_email = '%(to_email)s' % {'to_email': subscriber.user.email}
            send_mail(subject,
                      message,
                      from_email,
                      [to_email],
                      fail_silently=False)
        
        elif str(self.message_type) == 'SMS':
            if from_author.phone_number == '':
                from_author.phone_number = settings.TWILIO_DEFAULT_CALLERID
            twilio_client.sms.messages.create(
                body=self.message,
                to='+14172555504', # Replace with your phone number
#                from_=from_author.phone_number # Replace with your Twilio number
                from_=settings.TWILIO_DEFAULT_CALLERID # Replace with your Twilio number
            )
        
        elif str(self.message_type) == 'VML':
            message_cleaned = self.message.replace(' ', '%20')
            twilio_client.calls.create(
                to="+14172555504", # Any phone number
                from_=settings.TWILIO_DEFAULT_CALLERID, # Must be a valid Twilio number
                url="http://twimlets.com/message?Message=%s" % message_cleaned
            )
        else:
            raise ValidationError('Select a valid habitgram type: %s' % self.message_type)

    def __unicode__(self):
        return '%(habitgram)s - (%(message_type)s)' % \
            {'habitgram': self.title,
             'message_type': self.message_type}
    
    # Add the custom manager
    class Meta:
        class _Manager(models.Manager):
            use_for_related_fields = True
            
            def past(self, **kwargs):
                return self.filter(schedule__lte=datetime.now(), **kwargs)
            
            def future(self, **kwargs):
                return self.filter(schedule__gte=datetime.now(), **kwargs)

            def recent(self, **kwargs):
                return self.filter(is_active=True, **kwargs).order_by('-created')
    objects = Meta._Manager()


class Subscription(TimeStampedModel):
    """
       A class to store a program which a user can subscribe to
    """
    habitgram = models.ForeignKey(Habitgram, default=1, null=False, blank=False)
    subscriber = models.ForeignKey(HabitgramUser, default=1, null=False, blank=False)
    program_completed = models.NullBooleanField(default=False)
    coupon_code = models.CharField(default='', max_length=255)
    price = models.DecimalField(default=0.00, max_digits=8, decimal_places=2)
    currency = models.CharField(max_length=255, choices=CURRENCIES, default=CURRENCIES[0])
    quantity = models.IntegerField(default=1)
    total_billing_cycles = models.IntegerField(default=0)
    trial_ends_at = models.DateField(default=datetime.today().date)
    starts_at = models.DateField(default=datetime.today().date)
    first_renewal_date = models.DateField(default=datetime.today().date)

    def save(self, *args, **kwargs):
        if settings.RECURLY_ONLINE:
            subscription = recurly.Subscription()
            try:
                billing_info = BillingInfo.objects.get(user=self.subscriber.user)
            except BillingInfo.DoesNotExist:
                raise ValidationError('Please enter your billing information')
            try:
                subscription.plan_code = self.program.get_recurly_plan_code()
            except recurly.NotFoundError, e:
                raise ValidationError('Server error: %s\nPlease contact the administrator in the contact page.' % e)
            subscription.currency = self.program.currency
            account = recurly.Account.get(self.subscriber.get_recurly_account_code())
            try:
                subscription.account = account
                try:
                    subscription.save()
                except recurly.ValidationError, e:
                    if e in ['already_subscribed: subscription.base You already have a subscription to this plan.']:
                        super(Subscription, self).save(*args, **kwargs)
                        return
            except recurly.BadRequestError, e:
                raise ValidationError('Server error: %s\nPlease contact the administrator in the contact page.' % e)
        return super(Subscription, self).save(*args, **kwargs)
    
    def get_progress_url(self):
        return reverse('subscription-progress', args=[str(self.id)])
    
    def get_program_url(self):
        return self.program.get_absolute_url()

    def get_absolute_url(self):
        return reverse('subscription-progress', args=[str(self.id)])
    
    def __unicode__(self):
        return '%(subscriber)s subscribed to (%(habitgram)s)' % \
            {'subscriber': self.subscriber,
             'habitgram': self.habitgram}
    
    # Add the custom manager
    class Meta:
        verbose_name = "Subscription"
        verbose_name_plural = "Subscriptions"

        class _Manager(models.Manager):
            use_for_related_fields = True
            
            def ongoing(self, **kwargs):
                return self.filter(program_completed=False, **kwargs)
            
            def completed(self, **kwargs):
                return self.filter(program_completed=True, **kwargs)
            
            # Temporary aliases for backward compat with ScheduledHabitgram this absorbed.
            def past(self, **kwargs):
                return self.completed(**kwargs)
            
            def future(self, **kwargs):
                return self.ongoing(**kwargs)
    objects = Meta._Manager()


DAYS_OF_WEEK = (
    ('0', 'EVERY Day'),
    ('1', 'Monday'),
    ('2', 'Tuesday'),
    ('3', 'Wednesday'),
    ('4', 'Thursday'),
    ('5', 'Friday'),
    ('6', 'Saturday'),
    ('7', 'Sunday'),
)

class SubscriptionTime(models.Model):
    subscription = models.ForeignKey(Subscription)
    weekday = models.CharField(max_length=1, choices=DAYS_OF_WEEK)
    time = models.TimeField()
    
    class Meta:
        verbose_name = "Subscription Time"
        verbose_name_plural = "Subscription Times"
