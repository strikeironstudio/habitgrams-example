# events/views.py

from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.http.response import Http404
from django.shortcuts import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, FormView
from django.views.generic.edit import CreateView, UpdateView
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import get_object_or_404, redirect
from django.forms.models import inlineformset_factory
from django.utils.translation import ugettext as _


from extra_views import CreateWithInlinesView, UpdateWithInlinesView, InlineFormSet
from extra_views.generic import GenericInlineFormSet

import recurly

from users.models import (HabitgramUser, BillingInfo)

from core.forms import MultiForms

from .forms import SubscriptionForm, HabitgramForms
from .models import Subscription, SubscriptionTime, Habitgram

####### Programs
class ProgramListView(ListView):
    model = Habitgram
    template_name = 'programs/list.html'
    
    def get_context_data(self, **kwargs):
        context = super(ProgramListView, self).get_context_data(**kwargs)
        recent_programs = self.model.objects.recent().all()
        current_user = self.request.user
        if current_user.is_authenticated():
            habitgram_user = HabitgramUser.get_by_user_id(current_user.id)
            for program in recent_programs:
                try:
                    subscription = Subscription.objects.get(habitgram=program,
                                                        subscriber=habitgram_user)
                    program.subscribed = True
                    program.subscription = subscription
                except Subscription.DoesNotExist:
                    program.subscribed = False
                    
        context["programs"] = recent_programs
        return context

class ProgramAuthListView(ListView):
    model = Habitgram
    template_name = 'programs/auth_list.html'

    def get_context_data(self, **kwargs):
        context = super(ProgramAuthListView, self).get_context_data(**kwargs)
        current_user = self.request.user
        authored_programs = tuple()
        if current_user.is_authenticated():
            author = HabitgramUser.get_by_user_id(current_user.id)
            authored_programs = author.habitgram_set.all()
            for program in authored_programs:
                try:
                    subscription = Subscription.objects.get(habitgram=program,
                                                        subscriber=current_user)
                    program.subscribed = True
                    program.subscription = subscription
                except Subscription.DoesNotExist:
                    program.subscribed = False
        context["programs"] = authored_programs
        return context

class ProgramDetailView(DetailView):
    model = Habitgram
    template_name = 'programs/detail.html'
    
    def get_context_data(self, **kwargs):
        context = super(ProgramDetailView, self).get_context_data(**kwargs)
        current_user = self.request.user
        if current_user.is_authenticated():
            subscriber = HabitgramUser.get_by_user_id(current_user.id)
            try:
                subscription = Subscription.objects.get(habitgram=self.get_object(),
                                                        subscriber=subscriber)
                context['subscribed'] = True
                context['subscription'] = subscription
            except Subscription.DoesNotExist:
                context["subscribed"] = False
        return context

def program_unsubscribe(request, pk):
    u = request.user
    sub = get_object_or_404(Subscription, subscriber__user__id=u.id, habitgram__id=pk)
    next = reverse('program-detail', kwargs={'pk': sub.habitgram.id})
    sub.delete()
    return redirect(next)

class SubscriptionTimeInline(InlineFormSet):
    model = SubscriptionTime
    extra = 1

class ProgramSubscribeView(CreateWithInlinesView):
    model = Subscription
    inlines = [SubscriptionTimeInline,]
    template_name = 'programs/detail.html'
    form_class = SubscriptionForm
    user = None
    queryset = Habitgram.objects.all()

    def get_object(self):
        """CreateWithInlinesView sets self.object to None

        Since we're abusing queryset and pk (or otherwise abusing model), we have to hack it.
        """
        pk = self.kwargs.get(self.pk_url_kwarg, None)
        queryset = self.get_queryset()
        queryset = queryset.filter(pk=pk)
        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except ObjectDoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj

    def get_context_data(self, **kwargs):
        context = super(ProgramSubscribeView, self).get_context_data(**kwargs)
        current_user = self.request.user

        
        context["billing_info_registered"] = True
        program = self.get_object()
        context["habitgram"] = program
        form = None
        subscriber = HabitgramUser.get_by_user_id(current_user.id)

        context["subscribed"] = False
        try:
            subscription = Subscription.objects.get(habitgram=program, subscriber=subscriber)
            context["subscribed"] = True
            context["subscription"] = subscription
        except Subscription.DoesNotExist:
            form_class = self.get_form_class()
            form = self.get_form(form_class)
            context["form"] = form

        if form:
            form.action = reverse('program-subscribe', args=(program.id,))
            context["form"] = form

        context["current_user"] = current_user
        return context
    
    def forms_valid(self, form, inlines):
        """When the form is valid, register the user"""
        current_user = self.request.user
        subscriber = HabitgramUser.get_by_user_id(current_user.id)

        new_sub = form.save(commit=False)
        new_sub.habitgram = self.get_object()
        new_sub.subscriber = subscriber
        new_sub.save()
        
        for sub_time in inlines[0]:
            sub_time_item = sub_time.save(commit=False)
            sub_time_item.subscription = new_sub
            sub_time_item.save()

        self.success_url = reverse('program-detail', kwargs={'pk': new_sub.habitgram.id})
        return super(ProgramSubscribeView, self).forms_valid(form, inlines)

class SubscriptionUpdateView(UpdateWithInlinesView):
    model = Subscription
    inlines = [SubscriptionTimeInline,]
    template_name = 'programs/detail.html'
    form_class = SubscriptionForm

    def get_queryset(self, pk=None):
        current_user = self.request.user
        subscriber = HabitgramUser.get_by_user_id(current_user.id)
        return Subscription.objects.filter(subscriber=subscriber)
    
    def get_form(self, form_class):
        form = super(SubscriptionUpdateView, self).get_form(form_class)
        sub = self.get_object()
        form.action = reverse('program-sub-update', kwargs={'pk':sub.id})
        return form

    def get_context_data(self, **kwargs):
        context = super(SubscriptionUpdateView, self).get_context_data(**kwargs)
        current_user = self.request.user
        subscriber = HabitgramUser.get_by_user_id(current_user.id)
        sub = self.get_object()
        context["habitgram"] = sub.habitgram
        context["subscribed"] = True
        context["subscription"] = sub
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form.action = reverse('program-sub-update', args=(sub.id,))
        context["form"] = form
        context["current_user"] = current_user
        return context
    
    def forms_valid(self, form, inlines):
        """When the form is valid, register the user"""
        current_user = self.request.user
        subscriber = HabitgramUser.get_by_user_id(current_user.id)

        sub = form.save(commit=False)
        sub.habitgram = self.get_object().habitgram
        sub.subscriber = subscriber
        sub.save()
        
        for sub_time in inlines[0]:
            sub_time_item = sub_time.save(commit=False)
            sub_time_item.subscription = sub
            sub_time_item.save()

        self.success_url = reverse('program-detail', kwargs={'pk': sub.habitgram.id})
        return super(SubscriptionUpdateView, self).forms_valid(form, inlines)

class ProgramCreateView(CreateView):
    model = Habitgram
    form_class = MultiForms
    template_name = 'programs/create.html'

    def get_form_kwargs(self, author=None, initial=None):
        kwargs = super(ProgramCreateView, self).get_form_kwargs()
        #habitgram = Habitgram(author=author) if author is not None else Habitgram()
        kwargs.update({
            'forms': (HabitgramForms,
                      SubscriptionForm,
                      inlineformset_factory(Subscription, SubscriptionTime, extra=1)),
            'action': reverse_lazy("program-create"),
            'submit_text': "Save Habitgram",
        })
        
        return kwargs
    
    def get_context_data(self, **kwargs):
        context = super(ProgramCreateView, self).get_context_data(**kwargs)
        current_user = self.request.user
        if current_user.is_authenticated():
            author = HabitgramUser.get_by_user_id(current_user.id)
            context["form"] = self.get_form(self.get_form_class())
        else:
            context["form"] = None
        return context
    
    def form_valid(self, form):
        """
        If the form and formsets are valid, save the associated models.
        """
        self.object = None
        subscriber = None
        current_user = self.request.user
        if current_user.is_authenticated():
            subscriber = HabitgramUser.get_by_user_id(current_user.id)

        new_habitgram = form[0].save(commit=False)
        new_habitgram.author_id = current_user.id
        new_habitgram.save()

        new_sub = form[1].save(commit=False)
        new_sub.habitgram = new_habitgram
        new_sub.subscriber = subscriber
        new_sub.save()
        
        for sub_time in form[2]:
            sub_time_item = sub_time.save(commit=False)
            sub_time_item.subscription = new_sub
            sub_time_item.save()

        return HttpResponseRedirect(reverse('program-auth-list'))

class ProgramUpdateView(UpdateView):
    model = Habitgram
    form_class = HabitgramForms
    template_name = 'programs/edit.html'

    def get_form(self, form_class):
        kwargs = self.get_form_kwargs()
        form = form_class(**kwargs)
        form.action = reverse('program-update', kwargs={'pk':self.get_object().id})
        return form

####### Subscriptions
class SubscriptionListView(ListView):
    model = Subscription
    template_name = 'programs/sub_list.html'
    
    def get_context_data(self, **kwargs):
        context = super(SubscriptionListView, self).get_context_data(**kwargs)
        current_user = self.request.user
        current_subscriber = HabitgramUser.get_by_user_id(current_user.id)
        programs = []
        for sub in self.model.objects.ongoing(subscriber=current_subscriber).all():
            program = sub.habitgram
            program.subscribed = True
            program.subscription = sub
            programs.append(program)
        context["programs"] = programs

        return context


