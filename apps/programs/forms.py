# programs/forms.py

from django import forms
from django.contrib.auth import authenticate
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import redirect
from django.forms.models import BaseInlineFormSet
from django.forms.models import inlineformset_factory

from users.forms import SubscriberFormMixin
from users.models import (HabitgramUser, BillingInfo)

from .models import (Habitgram, Subscription, SubscriptionTime)


class HabitgramForms(forms.ModelForm):
    action = reverse_lazy("program-create")
    submit_text = "Save Habitgram"
    program = None

    class Meta:
        model = Habitgram
        fields = ('title', 'description', 'image', 'message_type', 'message')


class SubscriptionForm(SubscriberFormMixin, forms.ModelForm):
    """
       A form to handle user-registration for a program
    """
    
    #cvv_number = forms.IntegerField(required=True, label="CVV Number",
    #                                max_value=9999,
    #                                widget=forms.TextInput(attrs={'size': '4'}))
    action = reverse_lazy('program-subscribe')
    submit_text = "Subscribe"

    class Meta:
        model = Subscription
        fields = tuple()
        extra = 1
    
    def clean(self):
        cleaned_data = super(SubscriptionForm, self).clean()
        cvv_number = cleaned_data.get('cvv_number')
        if len(str(cvv_number)) < 4:
            self._errors["cvv_number"] = self.error_class(["The verification number must be 4 digits."])
        return cleaned_data
    
    # def register_user_to_program(self, user, program):
    #     billing_info = BillingInfo.get_billing_info_by_user_id(user.id)
    #     if billing_info.cvv_number == self.data['cvv_number']:
    #         self.set_subscriber(user)
    #         new_subscription, created = Subscription.objects.get_or_create(
    #                                         program=program,
    #                                         subscriber=self.subscriber)
    #         if created:
    #             new_subscription.save()
    #         else:
    #             raise forms.ValidationError('You have already subscribed to this program')
    #         return redirect(self.get_success_url())
    #     else:
    #         return redirect(self.get_error_url())

