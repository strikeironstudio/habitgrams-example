# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'HabitgramType'
        db.create_table(u'programs_habitgramtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('message_type', self.gf('django.db.models.fields.CharField')(default='---', unique=True, max_length=255)),
        ))
        db.send_create_signal(u'programs', ['HabitgramType'])

        # Adding model 'Program'
        db.create_table(u'programs_program', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(default='', null=True, blank=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(default='image', max_length=100)),
            ('duration', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('price', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=10, decimal_places=2)),
            ('currency', self.gf('django.db.models.fields.CharField')(default=('USD', 'USD'), max_length=255)),
            ('subscription_plan', self.gf('django.db.models.fields.CharField')(default=('per month', 'per month'), max_length=255)),
            ('is_active', self.gf('django.db.models.fields.NullBooleanField')(default=False, null=True, blank=True)),
        ))
        db.send_create_signal(u'programs', ['Program'])

        # Adding M2M table for field authors on 'Program'
        db.create_table(u'programs_program_authors', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('program', models.ForeignKey(orm[u'programs.program'], null=False)),
            ('author', models.ForeignKey(orm[u'users.author'], null=False))
        ))
        db.create_unique(u'programs_program_authors', ['program_id', 'author_id'])

        # Adding M2M table for field habitgrams on 'Program'
        db.create_table(u'programs_program_habitgrams', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('program', models.ForeignKey(orm[u'programs.program'], null=False)),
            ('habitgramtype', models.ForeignKey(orm[u'programs.habitgramtype'], null=False))
        ))
        db.create_unique(u'programs_program_habitgrams', ['program_id', 'habitgramtype_id'])

        # Adding model 'Subscription'
        db.create_table(u'programs_subscription', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('program', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['programs.Program'])),
            ('subscriber', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['users.Subscriber'])),
            ('program_completed', self.gf('django.db.models.fields.NullBooleanField')(default=False, null=True, blank=True)),
            ('coupon_code', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('price', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=8, decimal_places=2)),
            ('currency', self.gf('django.db.models.fields.CharField')(default=('USD', 'USD'), max_length=255)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('total_billing_cycles', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('trial_ends_at', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2013, 11, 12, 0, 0))),
            ('starts_at', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2013, 11, 12, 0, 0))),
            ('first_renewal_date', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2013, 11, 12, 0, 0))),
        ))
        db.send_create_signal(u'programs', ['Subscription'])


    def backwards(self, orm):
        # Deleting model 'HabitgramType'
        db.delete_table(u'programs_habitgramtype')

        # Deleting model 'Program'
        db.delete_table(u'programs_program')

        # Removing M2M table for field authors on 'Program'
        db.delete_table('programs_program_authors')

        # Removing M2M table for field habitgrams on 'Program'
        db.delete_table('programs_program_habitgrams')

        # Deleting model 'Subscription'
        db.delete_table(u'programs_subscription')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'programs.habitgramtype': {
            'Meta': {'object_name': 'HabitgramType'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message_type': ('django.db.models.fields.CharField', [], {'default': "'---'", 'unique': 'True', 'max_length': '255'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'programs.program': {
            'Meta': {'object_name': 'Program'},
            'authors': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['users.Author']", 'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.CharField', [], {'default': "('USD', 'USD')", 'max_length': '255'}),
            'description': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'habitgrams': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['programs.HabitgramType']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'default': "'image'", 'max_length': '100'}),
            'is_active': ('django.db.models.fields.NullBooleanField', [], {'default': 'False', 'null': 'True', 'blank': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '10', 'decimal_places': '2'}),
            'subscription_plan': ('django.db.models.fields.CharField', [], {'default': "('per month', 'per month')", 'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        u'programs.subscription': {
            'Meta': {'object_name': 'Subscription'},
            'coupon_code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.CharField', [], {'default': "('USD', 'USD')", 'max_length': '255'}),
            'first_renewal_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 11, 12, 0, 0)'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '8', 'decimal_places': '2'}),
            'program': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['programs.Program']"}),
            'program_completed': ('django.db.models.fields.NullBooleanField', [], {'default': 'False', 'null': 'True', 'blank': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'starts_at': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 11, 12, 0, 0)'}),
            'subscriber': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['users.Subscriber']"}),
            'total_billing_cycles': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'trial_ends_at': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 11, 12, 0, 0)'})
        },
        u'users.author': {
            'Meta': {'object_name': 'Author'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'default': "'+14172044985'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        u'users.subscriber': {
            'Meta': {'object_name': 'Subscriber'},
            'address': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'company_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '3', 'null': 'True', 'blank': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['programs']