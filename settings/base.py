"""Common settings and globals."""


from os.path import abspath, basename, dirname, join as join_path, normpath
from sys import path as sys_path



########## PATH CONFIGURATION

# Absolute filesystem path to the Django project directory:
DJANGO_ROOT = dirname(dirname(abspath(__file__)))

# Absolute filesystem path to the top-level project folder:
SITE_ROOT = DJANGO_ROOT

# Site name:
SITE_NAME = basename(DJANGO_ROOT)

# Add our project to our pythonpath, this way we don't need to type our project
# name in our dotted import paths:
sys_path.append(DJANGO_ROOT)
sys_path.append(join_path(DJANGO_ROOT, 'apps'))

########## END PATH CONFIGURATION



########## DEBUG CONFIGURATION

# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = False

# See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
TEMPLATE_DEBUG = DEBUG

########## END DEBUG CONFIGURATION



########## MANAGER CONFIGURATION

# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ('Vignesh', 'vignesh.msundaram@gmail.com'),
    ('Phillip', 'phillip@cx.com'),
    ('AndrewBC', 'abc@wizardware.tk'),
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

########## END MANAGER CONFIGURATION


########## DATABASE CONFIGURATION

# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

########## END DATABASE CONFIGURATION



########## GENERAL CONFIGURATION

# See: https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
TIME_ZONE = 'America/Chicago'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en-us'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

LOCALE_PATHS = normpath(join_path(SITE_ROOT, 'locale'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

########## END GENERAL CONFIGURATION



########## MEDIA CONFIGURATION

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = normpath(join_path(SITE_ROOT, 'media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'

########## END MEDIA CONFIGURATION



########## STATIC FILE CONFIGURATION

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = normpath(join_path(SITE_ROOT, 'collect-static'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
        normpath(join_path(SITE_ROOT, 'static')),
)
# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'ajaxify.finders.AjaxifyFinder',
)

########## END STATIC FILE CONFIGURATION



########## SECRET CONFIGURATION

# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Note: This key only used for development and testing.
SECRET_KEY = 'dqj_&v!7sqmcayfsrev^bu1-dbfn^bmtj=@eis==_5cxff4=0$'

########## END SECRET CONFIGURATION



########## TEMPLATE CONFIGURATION

# See: https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    
    "allauth.account.context_processors.account",
    "allauth.socialaccount.context_processors.socialaccount",
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
TEMPLATE_DIRS = (
    normpath(join_path(SITE_ROOT, 'templates')),
)

########## END TEMPLATE CONFIGURATION



########## MIDDLEWARE CONFIGURATION

# See: https://docs.djangoproject.com/en/dev/ref/settings/#middleware-classes
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

########## END MIDDLEWARE CONFIGURATION



########## URL CONFIGURATION

# See: https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = 'core.urls'

LOGIN_URL = '/accounts/login/'
LOGIN_REDIRECT_URL = '/'

LOGOUT_URL = '/accounts/logout/'
LOGOUT_REDIRECT_URL = '/'

########## END URL CONFIGURATION



########## APP CONFIGURATION

DJANGO_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'suit',
    'django.contrib.admin',
    'django.contrib.admindocs',
)

THIRD_PARTY_APPS = (
    #'south',
    'django_twilio',
    'ajaxify',
    'timezone_field',
)

# Apps specific for this project go here.
LOCAL_APPS = (
    'users', #      HabitgramUser
    'programs', #   Programs
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

########## END APP CONFIGURATION


########## CRON CONFIGURATION

#CRON_CLASSES = [
#    "players.cron.UpdatePlayerResources",
#    "players.cron.UpdatePlayerUnits"
#]

########## END CRON CONFIGURATION



########## TWILIO CONFIGURATION

TWILIO_ACCOUNT_SID = 'AC5f71788de7f565004be90db7635818c2'
TWILIO_AUTH_TOKEN = 'd178a3c1d37acfbf1691d5bf31d09040'

TWILIO_DEFAULT_CALLERID = '+14172044985'

########## END TWILIO CONFIGURATION




########## SUPERUSER CONFIGURATION

SUPERUSER = {'username': 'vignesh',
             'password': 'vigneshpass'}

########## END SUPERUSER CONFIGURATION



########## LOGGING CONFIGURATION

# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        }
    }
}

########## END LOGGING CONFIGURATION



########## WSGI CONFIGURATION

# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'wsgi.application'

########## END WSGI CONFIGURATION



########## RECURLY SETTINGS

RECURLY_SUBDOMAIN = 'habitgrams-com'
RECURLY_API_KEY = 'e39fa9237a5a4499b078a6f1a51ca27a'
RECURLY_JS_PRIVATE_KEY = 'ba1a89a9ed634783b5094151fdeec7d4'
RECURLY_DEFAULT_CURRENCY = 'USD'
RECURLY_ONLINE = True

########## END RECURLY SETTINGS

try:
    from social_settings import *
except ImportError:
    pass
