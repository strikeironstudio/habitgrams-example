# Local development dependencies.

-r base.txt

coverage==3.6
django-discover-runner==0.2.2
django-debug-toolbar==0.9.4
Sphinx==1.1.3
django-suit==0.2.4